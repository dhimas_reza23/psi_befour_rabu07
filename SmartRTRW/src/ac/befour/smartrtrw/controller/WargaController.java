/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.befour.smartrtrw.controller;

import ac.befour.smartrtrw.model.data.WargaModel;
import ac.befour.smartrtrw.model.pojo.Warga;
import ac.befour.smartrtrw.view.rt.WargaFrame;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author dhimas
 */
public class WargaController {
    public void start(){
        new WargaFrame().setVisible(true);
    }
    
    public List<Warga> getAllWarga(){
        WargaModel wargaModel = new WargaModel();
        return wargaModel.getAllWarga();
    }
    
    public void saveOrUpdateWarga(Warga warga){
        WargaModel model = new WargaModel();
        model.saveOrUpdateWarga(warga);
    }
     
    public List<Warga> getWargaById(String cari){
        WargaModel model = new WargaModel();
        return model.getWargaById(cari);
    }
     
    public void hapusWarga(Warga warga){
        WargaModel model = new WargaModel();
        model.hapusWarga(warga);
    }
}
