/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.befour.smartrtrw.controller;

import ac.befour.smartrtrw.view.rt.HomeFrame;

/**
 *
 * @author dhimas
 */
public class HomeController {
    HomeFrame homeFrame;

    public HomeController() {
        homeFrame = new HomeFrame();
    }
    
    public void start(){
        homeFrame.setVisible(true);
    }
}
