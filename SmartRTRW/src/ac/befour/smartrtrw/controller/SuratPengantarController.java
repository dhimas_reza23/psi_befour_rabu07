/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.befour.smartrtrw.controller;

import ac.befour.smartrtrw.model.data.SuratPengantarModel;
import ac.befour.smartrtrw.model.pojo.SuratPengantar;
import ac.befour.smartrtrw.view.rt.SuratPengantarFrame;
import ac.befour.smartrtrw.view.rw.SuratPengantarFrameRw;
import java.util.List;

/**
 *
 * @author dhimas
 */
public class SuratPengantarController {
   
    
    public void start(){
        new SuratPengantarFrame().setVisible(true);
    }
    
    public void startFrameRw(){
        new SuratPengantarFrameRw().setVisible(true);
    }
    
    public void startRW(String noSurat){
        new SuratPengantarFrameRw(noSurat).setVisible(true);
    }
    
    public List<SuratPengantar> getAllSuratPengantar(){
        SuratPengantarModel model = new SuratPengantarModel();
        return model.getAllSuratPengantar();
    }
    
    public List<SuratPengantar> getSuratPengantarByStat(String status){
        SuratPengantarModel model = new SuratPengantarModel();
        return model.getAllSuratPengantarByStat(status);
    }
    
    public void saveOrUpdateSuratPengantar(SuratPengantar sp){
        SuratPengantarModel model = new SuratPengantarModel();
        model.saveOrUpdateSuratPengantar(sp);
    }
     
    public List<SuratPengantar> getSuratPengantarById(String cari){
        SuratPengantarModel model = new SuratPengantarModel();
        return model.getSuratPengantarById(cari);
    }
     
    public void hapusSuratPengantar(SuratPengantar sp){
        SuratPengantarModel model = new SuratPengantarModel();
        model.hapusSuratPengantar(sp);
    }
}
