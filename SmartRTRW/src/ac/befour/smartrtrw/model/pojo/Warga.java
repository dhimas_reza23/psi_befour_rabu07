/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.befour.smartrtrw.model.pojo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dhimas
 */
@Entity
@Table(name = "warga")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Warga.findAll", query = "SELECT w FROM Warga w"),
    @NamedQuery(name = "Warga.findByNoKtp", query = "SELECT w FROM Warga w WHERE w.noKtp = :noKtp"),
    @NamedQuery(name = "Warga.findByNoKk", query = "SELECT w FROM Warga w WHERE w.noKk = :noKk"),
    @NamedQuery(name = "Warga.findByNama", query = "SELECT w FROM Warga w WHERE w.nama = :nama"),
    @NamedQuery(name = "Warga.findByJenisKelamin", query = "SELECT w FROM Warga w WHERE w.jenisKelamin = :jenisKelamin"),
    @NamedQuery(name = "Warga.findByTempatLahir", query = "SELECT w FROM Warga w WHERE w.tempatLahir = :tempatLahir"),
    @NamedQuery(name = "Warga.findByTanggalLahir", query = "SELECT w FROM Warga w WHERE w.tanggalLahir = :tanggalLahir"),
    @NamedQuery(name = "Warga.findByKewarganegaraan", query = "SELECT w FROM Warga w WHERE w.kewarganegaraan = :kewarganegaraan"),
    @NamedQuery(name = "Warga.findByAgama", query = "SELECT w FROM Warga w WHERE w.agama = :agama"),
    @NamedQuery(name = "Warga.findByAlamat", query = "SELECT w FROM Warga w WHERE w.alamat = :alamat"),
    @NamedQuery(name = "Warga.findByPekerjaan", query = "SELECT w FROM Warga w WHERE w.pekerjaan = :pekerjaan")})
public class Warga implements Serializable {

    @Basic(optional = false)
    @Column(name = "tanggal_lahir")
    private String tanggalLahir;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noKtp")
    private Collection<SuratPengantar> suratPengantarCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "noKtp")
    private String noKtp;
    @Basic(optional = false)
    @Column(name = "noKk")
    private String noKk;
    @Basic(optional = false)
    @Column(name = "nama")
    private String nama;
    @Basic(optional = false)
    @Column(name = "jenis_kelamin")
    private String jenisKelamin;
    @Basic(optional = false)
    @Column(name = "tempat_lahir")
    private String tempatLahir;
    @Basic(optional = false)
    @Column(name = "kewarganegaraan")
    private String kewarganegaraan;
    @Basic(optional = false)
    @Column(name = "agama")
    private String agama;
    @Basic(optional = false)
    @Column(name = "alamat")
    private String alamat;
    @Basic(optional = false)
    @Column(name = "pekerjaan")
    private String pekerjaan;

    public Warga() {
    }

    public Warga(String noKtp) {
        this.noKtp = noKtp;
    }

    public Warga(String noKtp, String noKk, String nama, String jenisKelamin, String tempatLahir, String tanggalLahir, String kewarganegaraan, String agama, String alamat, String pekerjaan) {
        this.noKtp = noKtp;
        this.noKk = noKk;
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.tempatLahir = tempatLahir;
        this.tanggalLahir = tanggalLahir;
        this.kewarganegaraan = kewarganegaraan;
        this.agama = agama;
        this.alamat = alamat;
        this.pekerjaan = pekerjaan;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }

    public String getNoKk() {
        return noKk;
    }

    public void setNoKk(String noKk) {
        this.noKk = noKk;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getKewarganegaraan() {
        return kewarganegaraan;
    }

    public void setKewarganegaraan(String kewarganegaraan) {
        this.kewarganegaraan = kewarganegaraan;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noKtp != null ? noKtp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Warga)) {
            return false;
        }
        Warga other = (Warga) object;
        if ((this.noKtp == null && other.noKtp != null) || (this.noKtp != null && !this.noKtp.equals(other.noKtp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ac.befour.smartrtrw.model.pojo.Warga[ noKtp=" + noKtp + " ]";
    }

    @XmlTransient
    public Collection<SuratPengantar> getSuratPengantarCollection() {
        return suratPengantarCollection;
    }

    public void setSuratPengantarCollection(Collection<SuratPengantar> suratPengantarCollection) {
        this.suratPengantarCollection = suratPengantarCollection;
    }


    
}
