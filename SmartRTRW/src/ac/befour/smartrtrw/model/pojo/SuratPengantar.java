/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.befour.smartrtrw.model.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dhimas
 */
@Entity
@Table(name = "surat_pengantar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SuratPengantar.findAll", query = "SELECT s FROM SuratPengantar s")
    , @NamedQuery(name = "SuratPengantar.findByNoSurat", query = "SELECT s FROM SuratPengantar s WHERE s.noSurat = :noSurat")
    , @NamedQuery(name = "SuratPengantar.findByJenis", query = "SELECT s FROM SuratPengantar s WHERE s.jenis = :jenis")
    , @NamedQuery(name = "SuratPengantar.findByTanggal", query = "SELECT s FROM SuratPengantar s WHERE s.tanggal = :tanggal")
    , @NamedQuery(name = "SuratPengantar.findByStatus", query = "SELECT s FROM SuratPengantar s WHERE s.status = :status")})
public class SuratPengantar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "noSurat")
    private String noSurat;
    @Basic(optional = false)
    @Column(name = "jenis")
    private String jenis;
    @Basic(optional = false)
    @Column(name = "tanggal")
    @Temporal(TemporalType.DATE)
    private Date tanggal;
    @Basic(optional = false)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "noKtp", referencedColumnName = "noKtp")
    @ManyToOne(optional = false)
    private Warga noKtp;

    public SuratPengantar() {
    }

    public SuratPengantar(String noSurat) {
        this.noSurat = noSurat;
    }

    public SuratPengantar(String noSurat,Warga noKtp, Date tanggal, String jenis, String status) {
        this.noSurat = noSurat;
        this.noKtp = noKtp;
        this.jenis = jenis;
        this.tanggal = tanggal;
        this.status = status;
    }

    public String getNoSurat() {
        return noSurat;
    }

    public void setNoSurat(String noSurat) {
        this.noSurat = noSurat;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Warga getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(Warga noKtp) {
        this.noKtp = noKtp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noSurat != null ? noSurat.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuratPengantar)) {
            return false;
        }
        SuratPengantar other = (SuratPengantar) object;
        if ((this.noSurat == null && other.noSurat != null) || (this.noSurat != null && !this.noSurat.equals(other.noSurat))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ac.befour.smartrtrw.model.pojo.SuratPengantar[ noSurat=" + noSurat + " ]";
    }
    
}
