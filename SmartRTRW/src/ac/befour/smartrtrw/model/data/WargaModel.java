/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.befour.smartrtrw.model.data;

import ac.befour.smartrtrw.model.pojo.Warga;
import ac.befour.smartrtrw.utilities.HibernateUtilities;
import java.util.List;
import org.hibernate.*;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author dhimas
 */
public class WargaModel {
    SessionFactory session = HibernateUtilities.getSessionFactory();
    
    public Session openSession(){
        return session.openSession();
    }
    
    public List<Warga> getAllWarga(){
        Session sess = this.openSession();
        Criteria c = sess.createCriteria(Warga.class).addOrder(Order.asc("noKtp"));
        List<Warga> dataWarga = c.list();
        return dataWarga;
    }
    
    public List<Warga> getWargaById(String cari){
        Session sess = this.openSession();
        Criteria c = sess.createCriteria(Warga.class).add(Restrictions.like("noKtp", cari, MatchMode.ANYWHERE));
        List<Warga> warga = c.list();
        return warga;
    }
    
    public void saveOrUpdateWarga(Warga warga){
        Session sess = this.openSession();
        Transaction t = sess.beginTransaction();
        sess.saveOrUpdate(warga);
        t.commit();
    }
    
    public void hapusWarga(Warga warga){
        Session sess = openSession();
        Transaction t = sess.beginTransaction();
        sess.delete(warga);
        t.commit();
    }
}
