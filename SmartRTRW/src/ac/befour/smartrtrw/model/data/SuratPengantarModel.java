/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.befour.smartrtrw.model.data;

import ac.befour.smartrtrw.model.pojo.SuratPengantar;
import ac.befour.smartrtrw.utilities.HibernateUtilities;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author dhimas
 */
public class SuratPengantarModel {
    SessionFactory session = HibernateUtilities.getSessionFactory();
     
    public Session openSession(){
        return session.openSession();
    }
     
    public List<SuratPengantar> getAllSuratPengantar(){
        Session sess = this.openSession();
        Criteria c = sess.createCriteria(SuratPengantar.class).addOrder(Order.asc("noSurat"));
        List<SuratPengantar> sp = c.list();
        return sp;
    }
    
    public List<SuratPengantar> getAllSuratPengantarByStat(String status){
        Session sess = this.openSession();
        Criteria c = sess.createCriteria(SuratPengantar.class).add(Restrictions.like("status", status, MatchMode.ANYWHERE));
        List<SuratPengantar> sp = c.list();
        return sp;
    }
    
    public List<SuratPengantar> getSuratPengantarById(String cari){
        Session sess = this.openSession();
        Criteria c = sess.createCriteria(SuratPengantar.class).add(Restrictions.like("noSurat", cari, MatchMode.ANYWHERE));
        List<SuratPengantar> sp = c.list();
        return sp;
    }
    
    public void saveOrUpdateSuratPengantar(SuratPengantar sp){
        Session sess = this.openSession();
        Transaction t = sess.beginTransaction();
        sess.saveOrUpdate(sp);
        t.commit();
    }
    
    public void hapusSuratPengantar(SuratPengantar sp){
        Session sess = openSession();
        Transaction t = sess.beginTransaction();
        sess.delete(sp);
        t.commit();
    }
}
