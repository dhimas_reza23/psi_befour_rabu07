-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2018 at 02:49 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartrtrw`
--

-- --------------------------------------------------------

--
-- Table structure for table `surat_pengantar`
--

CREATE TABLE `surat_pengantar` (
  `noSurat` varchar(32) NOT NULL,
  `noKtp` varchar(64) NOT NULL,
  `jenis` varchar(64) NOT NULL,
  `tanggal` date NOT NULL,
  `status` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_pengantar`
--

INSERT INTO `surat_pengantar` (`noSurat`, `noKtp`, `jenis`, `tanggal`, `status`) VALUES
('SP/RT01/RW04/1', '1212', 'ktp', '2018-01-01', 'sesuai'),
('SP/RT01/RW04/2', '7651', 'skck', '2018-02-02', 'sedang diproses');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(3) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(9) NOT NULL,
  `level` varchar(21) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`) VALUES
(1, 'rt', 'adminrt', 'rt'),
(2, 'rw', 'adminrw', 'rw');

-- --------------------------------------------------------

--
-- Table structure for table `warga`
--

CREATE TABLE `warga` (
  `noKtp` varchar(64) NOT NULL,
  `noKk` varchar(64) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `tempat_lahir` varchar(64) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `kewarganegaraan` varchar(64) NOT NULL,
  `agama` varchar(64) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `pekerjaan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warga`
--

INSERT INTO `warga` (`noKtp`, `noKk`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `kewarganegaraan`, `agama`, `alamat`, `pekerjaan`) VALUES
('1212', '0909', 'fajar januar wicaksi', 'Laki-laki', 'bandung', '1997-01-01', 'indonesia', 'islam', 'bandung', 'mahasiswa'),
('7651', '90000', 'Ismail', 'Laki-laki', 'Ciamis', '1997-02-02', 'Indonesia', 'islam', 'ciamis', 'mahasiswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `surat_pengantar`
--
ALTER TABLE `surat_pengantar`
  ADD PRIMARY KEY (`noSurat`),
  ADD KEY `id_warga` (`noKtp`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warga`
--
ALTER TABLE `warga`
  ADD PRIMARY KEY (`noKtp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `surat_pengantar`
--
ALTER TABLE `surat_pengantar`
  ADD CONSTRAINT `surat_pengantar_ibfk_3` FOREIGN KEY (`noKtp`) REFERENCES `warga` (`noKtp`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
